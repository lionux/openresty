# testing
# FROM openresty/openresty:1.15.8.3-2-stretch-fat
FROM lionus/openresty-base:latest
RUN /usr/local/openresty/luajit/bin/luarocks install luajson
# https://gitlab.com/craigbarnes/lua-sass
# RUN /usr/local/openresty/luajit/bin/luarocks install sass 
# RUN /usr/local/openresty/bin/opm install bungle/lua-resty-sass 
RUN /usr/local/openresty/bin/opm install golgote/lua-resty-info
RUN /usr/local/openresty/bin/opm install liyo/neturl